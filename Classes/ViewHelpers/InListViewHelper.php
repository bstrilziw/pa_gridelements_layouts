<?php

namespace Paints\PaGridelementsLayouts\ViewHelpers;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

/**
 * This view helper implements a condition for an item list
 * which is especially useful if some layouts should be considered
 *
 * = Examples =
 *
 * <code title="Basic usage">
 * {theme:inList(list: '1,2', item:data.layout, then: 'someClass', else: '')}"
 * </code>
 * <output>
 * If the field "layout" of the current page contains either 1 or 2, the string "someClass" is shown.
 * </output>
 */
class InListViewHelper extends AbstractConditionViewHelper {

    /**
     * Initialize
     *
     * @return void
     * @api
     */
    public function initializeArguments() {
        self::registerArgument('list', 'string', 'List to be evaluated', TRUE);
        self::registerArgument('item', 'string', 'Item to be found in list', TRUE);
    }

    /**
     * This method decides if the condition is TRUE or FALSE. It can be overridden in extending viewhelpers to adjust functionality.
     *
     * @param array $arguments ViewHelper arguments to evaluate the condition for this ViewHelper, allows for flexiblity in overriding this method.
     * @return bool
     */
    static protected function evaluateCondition($arguments = NULL) {
        return (GeneralUtility::inList($arguments['list'], $arguments['item']) || $arguments['list'] == $arguments['item']);
    }
}