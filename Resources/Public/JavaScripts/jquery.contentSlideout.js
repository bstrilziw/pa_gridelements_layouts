(function($) {
	$.widget( "pa.contentSlideout", {
		
		hiddenContent: null,

		options: {
			breakpoints: {
				small: 9999
			},
			textOpen: '+',
			textClose: '-',

			beforeWrapInner: '<span class="contentslideout-text-open">{tmpl-text-open}</span><span class="contentslideout-text-close">{tmpl-text-close}</span>',
			afterWrapInner: '<span class="contentslideout-text-open">{tmpl-text-open}</span><span class="contentslideout-text-close">{tmpl-text-close}</span>'
		},

		_create: function() {
			var that = this;
			setTimeout(function () {
				that.initReizeEvents();
				that._initSlideout();
			}, 100);
		},
		initReizeEvents: function () {
			var that = this;

			$(window).on('changed.zf.mediaquery', function () {
				that._resetSlideout();
				that._initSlideout();
			});
		},
		_resetSlideout: function() {
			if (this.hiddenContent !== null && this.hiddenContent.length) {
				this.hiddenContent.unwrap();
				this.hiddenContent = null;
			}
			this.element.find('.contentslideout').remove();
		},
		_initSlideout: function() {
			this._getBreakpointElements();
			this.hiddenContent = this.element.children(':nth-child(n+'+this._getBreakpointElements()+')').wrapAll('<div class="hiddencontent" />');
			this._addSlideoutButtons();
		},
		_addSlideoutButtons: function() {
			var that = this;

			if (this.hiddenContent !== null && this.hiddenContent.length) {
				this.hiddenContent.parent().before('<a href="#" class="contentslideout contentslideout-before">' + this.options.beforeWrapInner.replace(/\{tmpl-text-open\}/g, this.options.textOpen).replace(/\{tmpl-text-close\}/g, this.options.textClose) + '</a>');
				this.hiddenContent.parent().after('<a href="#" class="contentslideout contentslideout-after">' + this.options.afterWrapInner.replace(/\{tmpl-text-open\}/g, this.options.textOpen).replace(/\{tmpl-text-close\}/g, this.options.textClose) + '</a>');
				this.element.find('.contentslideout').unbind('click').click(function () {
						that._clickHandler();
						return false;
				});
			}
		},
		_getBreakpointElements: function() {
			var visibleElements = 9999;
			$.each(this.options.breakpoints, function (breakpoint, value) {
				if (matchMedia(Foundation.MediaQuery.get(breakpoint)).matches) {
					visibleElements = value;
				}
			});
			return visibleElements+1;
		},
		_clickHandler: function() {
			var that = this;

			$(that.element).toggleClass('js-contentslideout-open');
			return false;
		}
	});
})(jQuery);