temp.gridelements.fluidtemplate = FLUIDTEMPLATE
temp.gridelements.fluidtemplate {
	layoutRootPaths.100 = EXT:pa_gridelements_layouts/Resources/Private/Layouts/
	partialRootPaths.100 = EXT:pa_gridelements_layouts/Resources/Private/Partials/
	settings {
		breakpoints {
			small = small
			medium = medium
			large = large
			xlarge = xlarge
		}
	}
}

tt_content.gridelements_pi1.20.10.setup {
	1column < lib.gridelements.defaultGridSetup
	1column {
		cObject < temp.gridelements.fluidtemplate
		cObject {
			file = EXT:pa_gridelements_layouts/Resources/Private/Templates/1Column.html
		}
	}

	2column < lib.gridelements.defaultGridSetup
	2column {
		cObject < temp.gridelements.fluidtemplate
		cObject {
			file = EXT:pa_gridelements_layouts/Resources/Private/Templates/2Column.html
		}
	}

	3column < lib.gridelements.defaultGridSetup
	3column {
		cObject < temp.gridelements.fluidtemplate
		cObject {
			file = EXT:pa_gridelements_layouts/Resources/Private/Templates/3Column.html
		}
	}

	4column < lib.gridelements.defaultGridSetup
	4column {
		cObject < temp.gridelements.fluidtemplate
		cObject {
			file = EXT:pa_gridelements_layouts/Resources/Private/Templates/4Column.html
		}
	}

	6column < lib.gridelements.defaultGridSetup
	6column {
		cObject < temp.gridelements.fluidtemplate
		cObject {
			file = EXT:pa_gridelements_layouts/Resources/Private/Templates/6Column.html
		}
	}

	contentWrapper < lib.gridelements.defaultGridSetup
	contentWrapper {
		cObject < temp.gridelements.fluidtemplate
		cObject {
			file = EXT:pa_gridelements_layouts/Resources/Private/Templates/ContentWrapper.html
		}
	}
}

lib.gridelements.tt_content.innerWrapClass < tt_content.stdWrap.innerWrap.cObject
lib.gridelements.tt_content.innerWrapClass.default.20.stdWrap >
lib.gridelements.tt_content.innerWrapClass.default.10 >
lib.gridelements.tt_content.innerWrapClass.default.15 >
lib.gridelements.tt_content.innerWrapClass.default.30 >
lib.gridelements.tt_content.innerWrapClass.1 =< lib.gridelements.tt_content.innerWrapClass.default
lib.gridelements.tt_content.innerWrapClass.5 =< lib.gridelements.tt_content.innerWrapClass.default
lib.gridelements.tt_content.innerWrapClass.6 =< lib.gridelements.tt_content.innerWrapClass.default
lib.gridelements.tt_content.innerWrapClass.10 =< lib.gridelements.tt_content.innerWrapClass.default
lib.gridelements.tt_content.innerWrapClass.11 =< lib.gridelements.tt_content.innerWrapClass.default
lib.gridelements.tt_content.innerWrapClass.12 =< lib.gridelements.tt_content.innerWrapClass.default
lib.gridelements.tt_content.innerWrapClass.20 =< lib.gridelements.tt_content.innerWrapClass.default
lib.gridelements.tt_content.innerWrapClass.21 =< lib.gridelements.tt_content.innerWrapClass.default
lib.gridelements.tt_content.innerWrapClass.66 =< lib.gridelements.tt_content.innerWrapClass.default

# Remove standard element wrapping if gridelement is used
tt_content.stdWrap.innerWrap.if {
	value.field = CType
	equals = gridelements_pi1
	negate = 1
}

page.includeJSFooterLibs {
	contentSlideout = EXT:pa_gridelements_layouts/Resources/Public/JavaScripts/jquery.contentSlideout.js
}

### Content-Wrapper Options ###
### it is possible to set default-values for open and close-text from typoscript. Also you can set the button inner markup.
#
#tt_content.gridelements_pi1.20.10.setup.contentWrapper.cObject {
#	settings {
#		textOpen = Mehr erfahren
#		textClose = Schließen
#		beforeWrapInner = <span class="contentslideout-text-open">{tmpl-text-open}</span><span class="contentslideout-text-close">{tmpl-text-close}</span>
#		afterWrapInner = <span class="contentslideout-text-open">{tmpl-text-open}</span><span class="contentslideout-text-close">{tmpl-text-close}</span>
#	}
#}