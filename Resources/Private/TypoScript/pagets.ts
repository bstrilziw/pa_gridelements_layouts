tx_gridelements.setup {
	1column {
		icon = EXT:pa_gridelements_layouts/Resources/Public/Icons/1Column.png
		title = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:1col
		description = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:1col_descr
		frame = 200
		config {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:1col_left
							colPos = 101
						}
					}
				}
			}
		}
	}
	2column {
		pi_flexform_ds_file = EXT:pa_gridelements_layouts/Configuration/FlexForms/2Column.xml
		icon = EXT:pa_gridelements_layouts/Resources/Public/Icons/2Column.png
		title = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:2col
		description = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:2col_descr
		frame = 200
		config {
			colCount = 2
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:2col_left
							colPos = 201
						}
						2 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:2col_right
							colPos = 202
						}
					}
				}
			}
		}
	}
	3column {
		pi_flexform_ds_file = EXT:pa_gridelements_layouts/Configuration/FlexForms/3Column.xml
		icon = EXT:pa_gridelements_layouts/Resources/Public/Icons/3Column.png
		title = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:3col
		description = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:3col_descr
		frame = 300
		config {
			colCount = 3
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:3col_left
							colPos = 301
						}
						2 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:3col_mid
							colPos = 302
						}
						3 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:3col_right
							colPos = 303
						}
					}
				}
			}
		}
	}
	4column {
		pi_flexform_ds_file = EXT:pa_gridelements_layouts/Configuration/FlexForms/4Column.xml
		icon = EXT:pa_gridelements_layouts/Resources/Public/Icons/4Column.png
		title = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:4col
		description = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:4col_descr
		frame = 400
		config {
			colCount = 4
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:4col_left
							colPos = 401
						}
						2 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:4col_midl
							colPos = 402
						}
						3 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:4col_midr
							colPos = 403
						}
						4 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:4col_right
							colPos = 404
						}
					}
				}
			}
		}
	}
	6column {
		pi_flexform_ds_file = EXT:pa_gridelements_layouts/Configuration/FlexForms/6Column.xml
		icon = 
		title = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:6col
		description = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:6col_descr
		frame = 600
		config {
			colCount = 6
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:6col_1
							colPos = 601
						}
						2 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:6col_2
							colPos = 602
						}
						3 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:6col_3
							colPos = 603
						}
						4 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:6col_4
							colPos = 604
						}
						5 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:6col_5
							colPos = 605
						}
						6 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:6col_6
							colPos = 606
						}
					}
				}
			}
		}
	}
	contentWrapper {
		pi_flexform_ds_file = EXT:pa_gridelements_layouts/Configuration/FlexForms/ContentWrapper.xml
		icon = EXT:pa_gridelements_layouts/Resources/Public/Icons/ContentWrapper.png
		title = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:content_wrapper
		description = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:content_wrapper_descr
		frame = 300
		config {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:content_wrapper_content
							colPos = 11001
						}
					}
				}
			}
		}
	}
}

TCEFORM.tt_content.pi_flexform.gridelements_pi1.gridlayout.breakatbehaviour {
	addItems {
		small = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:breakpoint_small
		medium = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:breakpoint_medium
		large = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:breakpoint_large
	}
}

TCEFORM.tt_content.pi_flexform.gridelements_pi1.gridlayout.rowcollapse {
	addItems {
		small = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:breakpoint_small
		medium = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:breakpoint_medium
		large = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:breakpoint_large
		xlarge = LLL:EXT:pa_gridelements_layouts/Resources/Private/Language/locallang_db.xlf:breakpoint_xlarge
	}
}