﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
Gridelements Layouts
=============================================================

.. only:: html

	:Classification:
		pa_gridelements_layouts

	:Version:
		|release|

	:Language:
		en

	:Description:
		Adds typical gridelements column layouts

	:Keywords:
		comma,separated,list,of,keywords

	:Copyright:
		2014

	:Author:
		Heiko Kromm

	:Email:
		h.kromm@paints.de

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
	:glob:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Targets
